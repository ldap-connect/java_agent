package Service;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.json.JSONObject;

import dto.User;
import main.ADClient;

public class UserServiceImpl implements UserServiceIface{

	ADClient adClient = new ADClient();
	
	public List<User> getAllUsers()
	{
		List<User> userList = new ArrayList<>();
		
		try {
			System.out.println("-->try :: 1");
		
			String searchFilter="(objectClass=person)";
			String[] requiredAttributes={"sn","cn","employeeNumber","mail"  };
			
			SearchControls controls=new SearchControls();
			
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			controls.setReturningAttributes(requiredAttributes);
			NamingEnumeration users= null;
			//NamingEnumeration users=getContext().search("ou=users,o=Company", searchFilter, controls);
		
			try {
				if(adClient.getContext()!=null)
				{
				 users=adClient.getContext().search("dc=example,dc=com", searchFilter, controls);
				}
			}
			catch(Exception ex)
			{
				System.out.println("catch ::1" +ex);
				
			}
			
			
			SearchResult searchResult=null;
			String commonName=null;
			String surName=null;
			String employeeNum=null;
			String email = null;
			
			while(users.hasMore())
			{
				User user = new User();
				
				searchResult=(SearchResult) users.next();
				
				Attributes attr=searchResult.getAttributes();
				System.out.println("-->try :: 2");
				System.out.println("while :: Email :" +email);
				
				commonName=attr.get("cn").get(0).toString();
				surName=attr.get("sn").get(0).toString();
				employeeNum=attr.get("employeeNumber").get(0).toString();
				email =attr.get("mail").toString();
				String DN =searchResult.getNameInNamespace();
				
				user.setCommonName(commonName);
				user.setSurName(surName);
				user.setEmployeeNum(employeeNum);
				user.setEmail(email);
				user.setDN(DN);
				
				System.out.println("Name = "+commonName);
				System.out.println("Surname  = "+surName);
				System.out.println("Employee number = "+employeeNum);
				System.out.println("Email = "+email);
				
				System.out.println("-------------------------------------------");
				
				userList.add(user);
			}
			//System.out.println(context.getEnvironment());
			adClient.getContext().close();
			 
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userList;
	}
	
	public void createUser1(JSONObject json) throws NamingException 
	{
		System.out.println("createUser()1 :: 1 ::");
		System.out.println("createUser()1 ::" +json.get("commonName"));
		
		Attributes container = new BasicAttributes();

		// Create the objectclass to add
		Attribute objClasses = new BasicAttribute("ObjectClass");
		objClasses.add("inetOrgPerson");
		
		String snString = (String) json.get("commonName")+" "+ (String) json.get("surName");
		
		Attribute sn = new BasicAttribute("sn");
		sn.add( json.get("surName"));
		
		Attribute cn = new BasicAttribute("cn");
		cn.add(snString);
		
		Attribute mail = new BasicAttribute("mail");
	
		mail.add(json.get("email"));
		//Attribute empNumber = new BasicAttribute("employeeNumber");
		//empNumber.add(user.getEmployeeNum());
		
		container.put(objClasses);
		container.put(cn);
		container.put(sn);
		container.put(mail);
		
		//getContext().createSubcontext("employeeNumber=201,ou=users,o=Company",container);
		//String entryDN = "employeeNumber="+json.get("employeeNumber") + ",ou=users,o=Company";
		String entryDN = "employeeNumber="+json.get("employeeNumber") + ",dc=example,dc=com";
		
		adClient.getContext().createSubcontext(entryDN,container);
		
		System.out.println("created successfully..");

	}
}
