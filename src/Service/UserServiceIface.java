package Service;

import java.util.List;

import javax.naming.NamingException;

import org.json.JSONObject;

import dto.User;

public interface UserServiceIface {

	public void createUser1(JSONObject json) throws NamingException;
	public List<User> getAllUsers();
	
}
