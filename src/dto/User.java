package dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ser.std.SerializableSerializer;

public class User extends SerializableSerializer{
	
	@JsonProperty("commonName")
	public String commonName;
	
	@JsonProperty("surName")
	public String surName;
	
	@JsonProperty("employeeNumber")
	public String employeeNumber;
	
	public String email;
	
	public String DN;
	
	
	
	public String getDN() {
		return DN;
	}
	public void setDN(String dN) {
		DN = dN;
	}
	public String getCommonName() {
		return commonName;
	}
	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}
	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	public String getEmployeeNum() {
		return employeeNumber;
	}
	public void setEmployeeNum(String employeeNum) {
		this.employeeNumber = employeeNum;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
