package main;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;import com.fasterxml.jackson.databind.introspect.VisibilityChecker;

import Service.UserServiceIface;
import Service.UserServiceImpl;
import dto.User;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter.Listener;

public class ADClient {

	final static String IO_URL = "http://localhost:3000";
	
	public final static String LDAP_URL = "ldap://localhost:10389";
	
	static JSONObject ldapError = new JSONObject();
	
	public static DirContext getContext() throws NamingException
	{
		DirContext dirContext =null;
		try {
			
			System.out.println("ADClient :: getContext():: 1");
			Hashtable env = new Hashtable();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, LDAP_URL);
			env.put(Context.SECURITY_AUTHENTICATION, "none");
			env.put(Context.SECURITY_PRINCIPAL, "uid=admin,ou=system");
			env.put(Context.SECURITY_CREDENTIALS, "secret");
			
			 dirContext = new InitialDirContext(env);
		}
		catch(Exception e)
		{
			System.out.println("ADClinet :: getContext() ::catch :: 1");
			e.printStackTrace();
			ldapError.put("status","down");
		}
		
		return dirContext;
	}
	
	
	public static void main(String args[]) throws NamingException, URISyntaxException
	{
		Method enumMethod =null;
		
		Socket socket = IO.socket( IO_URL);
		socket.connect();
		
		socket.on("emitEvent", new Listener() {
			
			@Override
			public void call(Object... arg) {
				
				JSONObject response = new JSONObject();
				
				System.out.println(">> 1 ::emit event..");
				System.out.println("2 ::"+arg[0]);
				JSONObject jsonObj =(JSONObject)arg[0];
				
				String method = jsonObj.get("method").toString();
			
				System.out.println(jsonObj.get("method") + ":Method called");
				System.out.println("Arg: "+jsonObj.get("arg"));
				
				List<User> list =null ;
				UserServiceIface userServiceIface =new UserServiceImpl();
				
				if(method.equals(enumMethod.getUsers.toString()))
				{
					
					list =userServiceIface.getAllUsers();
					
					response.put("response", list);
					
					//socket.emit("callMethod", new JSONObject().put("response", list));
				}	
			
				if(method.equals(enumMethod.createUser.toString()))
				{
					JSONObject error = new JSONObject();
					User usr =new User();
					ADClient adclient = new ADClient();
				
					JSONObject jsonchild = (JSONObject)jsonObj.get("arg");
					System.out.println(jsonchild);
					
					try {
						
						userServiceIface.createUser1(jsonchild);
						response.put("response","Success" );
						
					} catch (NamingException e) {
		
						e.getMessage();
						error.put("error", e.getMessage());
					}
				
					if(error!=null) {
						socket.emit("callMethod", error);
					}
					
					}
				
				
				
	socket.emit("callMethod" ,  response);
				
				}
			
		});
			
	
	
	
	}
}
